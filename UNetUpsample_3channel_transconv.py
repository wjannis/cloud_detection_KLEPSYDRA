
import tensorflow as tf
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2DTranspose
from tensorflow.keras.layers import concatenate
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.layers import SeparableConv2D
from tensorflow.keras.layers import UpSampling2D
from tensorflow.keras.layers import ReLU

def EncoderMiniBlock(inputs, n_filters=32, dropout_prob=0.3, max_pooling=True):

    if max_pooling:
        inputs = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(inputs)

    conv = Conv2D(n_filters,
                  3,  # Kernel size
                  #activation='relu',
                  padding='same',
                  kernel_initializer='HeNormal')(inputs)

    conv = BatchNormalization()(conv, training=False)
    conv = ReLU()(conv)

    conv = Conv2D(n_filters,
                  3,  # Kernel size
                  #activation='relu',
                  padding='same',
                  kernel_initializer='HeNormal')(conv)

    conv = BatchNormalization()(conv, training=False)
    conv = ReLU()(conv)


    if dropout_prob > 0:
        conv = tf.keras.layers.Dropout(dropout_prob)(conv)

    # if max_pooling:
    #     next_layer = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv)
    # else:
    #     next_layer = conv
    #
    # skip_connection = conv

    return conv


def DecoderMiniBlock(prev_layer_input, skip_layer_input, in_filters=32, out_filters=32):

    up = Conv2DTranspose(
         in_filters,
         (3, 3),  # Kernel size
         strides=(2, 2),
         padding='same')(prev_layer_input)
    # height, width = prev_layer_input.shape[1:3]
    # up = Resizing(height * 2, width * 2)(prev_layer_input)

    #up = UpSampling2D(interpolation='bilinear')(prev_layer_input)
    merge = concatenate([up, skip_layer_input], axis=3)

    conv = Conv2D(in_filters,
                  3,  # Kernel size
                  #activation='relu',
                  padding='same',
                  kernel_initializer='HeNormal')(merge)
    conv = BatchNormalization()(conv, training=False)
    conv = ReLU()(conv)

    conv = Conv2D(out_filters,
                  3,  # Kernel size
                  #activation='relu',
                  padding='same',
                  kernel_initializer='HeNormal')(conv)
    conv = BatchNormalization()(conv, training=False)
    conv = ReLU()(conv)

    return conv


def UNetCompiled(input_size=(None, None, 3), n_filters=32, n_classes=1):

    inputs = Input(input_size, batch_size=1)

    cblock1 = EncoderMiniBlock(inputs, n_filters, dropout_prob=0, max_pooling=False)
    cblock2 = EncoderMiniBlock(cblock1, n_filters * 2, dropout_prob=0, max_pooling=True)
    cblock3 = EncoderMiniBlock(cblock2, n_filters * 4, dropout_prob=0, max_pooling=True)
    cblock4 = EncoderMiniBlock(cblock3, n_filters * 8, dropout_prob=0.3, max_pooling=True)
    cblock5 = EncoderMiniBlock(cblock4, n_filters * 8, dropout_prob=0.3, max_pooling=True)
    ublock = DecoderMiniBlock(cblock5, cblock4, n_filters * 8, n_filters * 8//2)
    ublock = DecoderMiniBlock(ublock, cblock3, n_filters * 4, n_filters * 4//2)
    ublock = DecoderMiniBlock(ublock, cblock2, n_filters * 2, n_filters)
    # ublock = DecoderMiniBlock(ublock, cblock1, n_filters)
    ublock = DecoderMiniBlock(ublock, cblock1, n_filters, n_filters)

    # conv9 = Conv2D(n_filters,
    #                3,
    #                activation='relu',
    #                padding='same',
    #                kernel_initializer='he_normal')(ublock)

    conv10 = Conv2D(n_classes, 1, padding='same', activation='sigmoid')(ublock)

    # Define the model
    model = tf.keras.Model(inputs=inputs, outputs=conv10)

    return model
