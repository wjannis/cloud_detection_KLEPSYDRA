import tensorflow as tf
import numpy as np
from UNetUpsample_3channel_transconv import UNetCompiled as UNet3C
from UNetUpsample_4channel_transconv import UNetCompiled as UNet4C

from PIL import Image
import matplotlib.pyplot as plt


if __name__ == '__main__':

    models = ["cloud_95_unet32alpha0_125_transconv_3channel",
              "cloud_95_unet32alpha1_0_transconv_3channel",
              "cloud_95_unet32alpha0_125_transconv_4channel",
              "cloud_95_unet32alpha1_0_transconv_4channel"]
    num_filters = [4, 32, 4, 32]
    networks = [UNet3C, UNet3C, UNet4C, UNet4C]
    channels = [3, 3, 4, 4]

    for m, f, n, c in zip(models, num_filters, networks, channels):
        keras_model = tf.keras.saving.load_model("./models/" + m + "/checkpoints_acc/ckp")
        keras_model.summary()
        newModel = n(input_size=[384,384,c], n_filters=f)
        newModel.summary()
        newModel.set_weights(keras_model.get_weights())

        def representative_data_gen():
            for i in range(10):
                data = tf.random.uniform(shape=(1, 384, 384, c), dtype=tf.float32)
                yield [data]

        converter_float = tf.lite.TFLiteConverter.from_keras_model(newModel)
        tflite_model = converter_float.convert()

        converter_int = tf.lite.TFLiteConverter.from_keras_model(newModel)
        converter_int.optimizations = [tf.lite.Optimize.DEFAULT]
        converter_int.representative_dataset = representative_data_gen
        converter_int.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
        tflite_model_quant = converter_int.convert()

        import pathlib

        tflite_models_dir = pathlib.Path("./models/" + m + "/tflite_models/")
        tflite_models_dir.mkdir(exist_ok=True, parents=True)

        # # Save the unquantized/float model:
        tflite_model_file = tflite_models_dir / "model.tflite"
        tflite_model_file.write_bytes(tflite_model)
        # # Save the quantized model:
        tflite_model_quant_file = tflite_models_dir / "model_quant.tflite"
        tflite_model_quant_file.write_bytes(tflite_model_quant)

        img=np.array([Image.open("scene_4_1K.png")])
        input_data = (img[:,:384,:384,:c]/255.).astype(np.float32)


        interpreter = tf.lite.Interpreter(model_path="./models/" + m
                                                     +"/tflite_models/model_quant.tflite")
        interpreter.allocate_tensors()

        # Get input and output tensors.
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()

        # Test the model on random input data.
        input_shape = input_details[0]['shape']
        # input_data = np.array(np.random.random_sample(input_shape), dtype=np.float32)
        interpreter.set_tensor(input_details[0]['index'], input_data)

        interpreter.invoke()

        # The function `get_tensor()` returns a copy of the tensor data.
        # Use `tensor()` in order to get a pointer to the tensor.
        output_data = interpreter.get_tensor(output_details[0]['index'])

        plt.imshow(output_data[0])
        plt.show()
