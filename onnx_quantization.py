import tensorflow as tf
import numpy as np
import onnxruntime as ort
from onnxruntime.quantization import QuantType, QuantFormat

from PIL import Image
import matplotlib.pyplot as plt

class dr:
    def __init__(self, i, c):
        self.enum_data = None
        self.datasize = 10
        self.i = i
        self.c = c

    def representative_data_gen(self):
        # for input_value in np.random.random([10,384,384,3]):
        #     yield [input_value]
        for i in range(10):
            data = tf.random.uniform(shape=(1, 384, 384, self.c), dtype=tf.float32)
            # data = tf.cast(data, dtype=tf.uint8)
            yield np.array(data)

    def get_next(self):
        if self.enum_data is None:
            self.enum_data = iter(
                [{"serving_default_input_"+i+":0": nhwc_data} for nhwc_data in self.representative_data_gen()]
            )
        return next(self.enum_data, None)

if __name__ == '__main__':

    models = ["cloud_95_unet32alpha0_125_transconv_3channel",
              "cloud_95_unet32alpha1_0_transconv_3channel",
              "cloud_95_unet32alpha0_125_transconv_4channel",
              "cloud_95_unet32alpha1_0_transconv_4channel"]
    num_filters = [4, 32, 4, 32]
    channels = [3, 3, 4, 4]
    inputs = ['1', '2', '3', '4']

    for m, f, c, i in zip(models, num_filters, channels, inputs):
        model = "./models/" + m + "/onnx_models/model.onnx"
        model_quant = "./models/" + m + "/onnx_models/model.quant.onnx"
        datareader = dr(i, c)

        quant_m = ort.quantization.quantize_static(model,
                                                  model_quant,
                                                  datareader,
                                                  quant_format=QuantFormat.QOperator,
                                                  activation_type=QuantType.QUInt8,
                                                  weight_type=QuantType.QUInt8)

        img=np.array([Image.open("scene_4_1K.png")])
        input_data = (img[:,:384,:384,:c]/255.).astype(np.float32)

        ort_sess = ort.InferenceSession(model_quant)
        outputs = ort_sess.run(None, {'serving_default_input_'+i+':0': input_data})

        # Print Result
        output_data = outputs[0]

        plt.imshow(output_data[0])
        plt.show()

        ort_sess = ort.InferenceSession(model)
        outputs = ort_sess.run(None, {'serving_default_input_'+i+':0': input_data})

        # Print Result
        output_data_onnx = outputs[0]

        plt.imshow(output_data[0])
        plt.show()
